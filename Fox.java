public class Fox {
	private String breed;
	private String name;
	private String sound;
	private String color;
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getBreed(){
		return this.breed;
	}
	public String getName(){
		return this.name;
	}
	public String getSound(){
		return this.sound;
	}
	public String getColor(){
		return this.color;
	}
	
	public Fox(String breed, String name, String sound, String color){
		this.breed = breed;
		this.name = name;
		this.sound = sound;
		this.color = color;
		
	}
	
	public void makeSound() {
		System.out.println("What does the " + this.breed + " " + this.name + " say?");
		System.out.println("\n" + this.sound);
	}
	public void describeBreed() {
		System.out.println(this.name + " is a " + this.breed + ".");
		System.out.println( this.name + " is " +  this.color);
	}
}